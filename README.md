# sch-gateway

## Building

To build you can use either the Shell or Python scripts included in the `build/` folder.

Both scripts MUST be launched from the root repository folder (the one containing the .git folder).

The script will generate `src/variables.h` (not tracked by git) which contains two `String` variables:

* `branch` for the git branch used as source
* `version` a string to identify the build (date and hash of latest commit)

You will need to create (or update) the `src/variables.h` file yourself if you plan on building the source without the scripts.

### build.py

The Python script requires Python3.5 or above and the following PyPi modules:

* requests
* gitpython

To install these modules run `pip install --user [module name]` (without brackets).

The script can be run directly (`$ build/build.py`) if your system supports it, or by opening it with the Python interpreter (`$ python3 build/build.py`).

### build.sh

The Shell script requires that your system has `git` and `particle-cli` installed and available in your command line.

For `particle-cli` installation refer to its documentation: [Particle Tutorials | Command Line](https://docs.particle.io/tutorials/developer-tools/cli/)

You can also use Node.js to install it, but remember to add it to your `$PATH`.

The script can be run directly (`$ build/build.sh`) if your system supports it, or by opening it with Shell (`$ sh build/build.sh`).

The Shell script is also compatible with Bash and ZSH.
