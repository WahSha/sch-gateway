#!python3
from json import load
from os import listdir, makedirs, path
from sys import argv, exit

import requests
from git import Commit, Repo

host: str = "https://api.particle.io"
platform_ids: dict = {
    "argon": "12",
    "boron": "13",
    "xenon": "14",
    "12": "argon",
    "13": "boron",
    "14": "xenon",
}

token: str = ""

repo: Repo = Repo(".")


def get_token(username: str, password: str):
    url: str = "/".join([host, "oauth/token"])
    data: dict = {"grant_type": "password", "username": username, "password": password}

    res: requests.Response = requests.post(
        url, auth=("particle", "particle"), data=data
    )

    if res.status_code == 200:
        return res.json()["access_token"]
    else:
        print("Authentication error")


def build_variables(settings: list) -> str:
    branch: str = str(repo.active_branch)

    commit: Commit = repo.commit("HEAD")
    commit_date: str
    commit_hash: str

    commit_date = commit.committed_datetime.strftime("%Y-%m-%d")
    commit_hash = commit.hexsha[:7]

    with open("src/variables.h", "w") as f:
        f.write("// GENERATED FILE\n// DO NOT EDIT\n\n")
        for setting in settings:
            f.write(f"#{'undef' if setting.startswith('!') else 'define'} ")
            f.write(f"{setting[1:] if setting.startswith('!') else setting}\n")
        if settings:
            f.write("\n")
        f.write(f'String branch = "{branch}";\n')
        f.write(f'String version = "{commit_date}_{commit_hash}";\n')

    return f"{commit_date}_{commit_hash}"


def compile(platform: str) -> str:
    if platform not in platform_ids:
        print("Unknown platform:", platform)
        return ""

    url: str = "/".join([host, "v1", "binaries"])
    auth: dict = {"Authorization": f"Bearer {token}"}
    files: dict = {
        "product_id": (None, platform_ids[platform]),
        "file": ("project.properties", open("project.properties", "rb")),
    }

    print("Including:")
    print("    project.properties")
    for i, f in enumerate(listdir("src"), 1):
        files[f"file{i}"] = (path.join("src", f), open(path.join("src", f), "rb"))
        print("    " + path.join("src", f))

    print()

    res = requests.post(url, headers=auth, files=files)

    if res.json()["ok"]:
        print("Compile successful")
        return res.json()["binary_url"]
    else:
        print(res.json()["output"])
        print("\n".join(res.json()["errors"]))
        return ""


def download(url: str, bin: str):
    url = host + url
    auth: dict = {"Authorization": f"Bearer {token}"}

    makedirs(path.dirname(bin), exist_ok=True)

    stream: requests.Response = requests.get(url, headers=auth, stream=True)

    if stream.status_code != 200:
        print("   " + str(stream.json()))
        exit(1)

    with open(bin, "wb") as f:
        for chunk in stream.iter_content(chunk_size=1024):
            f.write(chunk)


def flash(device: str, binary: str):
    url: str = "/".join([host, "v1", "devices", device])
    auth: dict = {"Authorization": f"Bearer {token}"}
    files: dict = {"file_type": (None, "binary"), "file": (binary, open(binary, "rb"))}

    res = requests.put("/".join([url, "ping"]), headers=auth)

    if not res.json()["online"]:
        print("    Not online")
        return

    res = requests.put(url, headers=auth, files=files)

    print("    " + res.json()["status"])


if __name__ == "__main__":
    # if repo.is_dirty() and "test" not in argv[1:]:
    #     print("Working tree is not clean. Commit changes before building")
    #     exit(1)
    # elif path.isfile(path.join("build", "build.json")):
    #     print("No deploy instructions")
    #     exit(1)

    if "flash" in argv[1:]:
        argv.append("compile")

    if "compile" not in argv[1:]:
        argv.append("compile")

    ###############

    credentials: dict = {}

    if path.isfile(path.join("build", "credentials.json")):
        cr_file = open(path.join("build", "credentials.json"), "r")
        credentials = load(cr_file)
        cr_file.close()

    print("# Login")
    if "username" in credentials and "password" in credentials:
        print("Username:", credentials["username"])
        print("Password:", credentials["password"])
    else:
        credentials["username"] = input("Username: ")
        credentials["password"] = input("Password: ")

    token = get_token(credentials["username"], credentials["password"])
    if not token:
        exit(1)

    print()

    ###############

    build: dict = {}

    bl_file = open(path.join("build", "build.json"), "r")
    build = load(bl_file)
    bl_file.close()

    if "test" in argv[1:]:
        print("# Testing")
        print("Platform:", build["test"]["platform"])
        print("Settings:", build["test"]["settings"])
        build_variables(build["test"]["settings"])
        compile(build["test"]["platform"])
        exit(0)

    print("# Compile")

    for product in build["products"]:
        name: str = product["name"]
        platform: str = product["platform"]
        settings: list = product["settings"]
        devices: list = product["devices"]

        version = build_variables(settings)
        binary_path = path.join("bin", f"{platform}_{version}_{name}.bin")

        print("Name    :", name)
        print("Platform:", platform)
        print("Settings:", settings)
        print("Devices :", devices)
        print("Version :", version)

        if not path.isfile(binary_path):
            binary_url = compile(platform)
            if not binary_url:
                continue

            download(binary_url, binary_path)
        else:
            print()

        print(f"Saved firmware to: {binary_path}")

        if "flash" not in argv[1:]:
            continue
        else:
            print()

        for device in devices:
            print("## Flashing to:", device)
            flash(device, binary_path)
