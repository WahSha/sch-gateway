#ifndef __GAS_SENSOR_H__
#define __GAS_SENSOR_H__

#include <math.h>

class MQ2
{
  private:
    float Ro = 10.0;
    float Rl = 5.0;
    float RoAir = 9.83;
    float LPGCurve[3] = { 2.3, 0.21, -0.47 };
    float COCurve[3] = { 2.3, 0.72, -0.34 };
    float SmokeCurve[3] = { 2.3, 0.53, -0.44 };
    uint8_t pin;
    void calibrate(void);
  public:
    MQ2(uint8_t);
    float getRatio(void);
    int getPpmLPG(void);
    int getPpmCO(void);
    int getPpmSmoke(void);
};


MQ2::MQ2(uint8_t pinArg) {
  pin = pinArg;
  this -> calibrate();
}

void MQ2::calibrate()
{
  float Ro_tmp = 0;

  for (char i = 0; i < 100; i++) {
    float raw_adc = analogRead(pin);
    Ro_tmp += (Rl * (4095 - raw_adc) / raw_adc);
    delay(2);
  }
  Ro_tmp /= 100;

  Ro_tmp /= RoAir;

  Ro = Ro_tmp;
}

float MQ2::getRatio()
{
  float Rs = 0;
  float ratio;

  for (char i = 0; i < 100; i++) {
    float raw_adc = analogRead(pin);
    Rs += (Rl * (4095 - raw_adc) / raw_adc);
    delay(2);
  }

  Rs /= 100;

  ratio = Rs / Ro;

  return ratio;
}

int MQ2::getPpmLPG()
{
  float ratio = this -> getRatio();
  float percentage = pow(10, ((log(ratio - LPGCurve[1]) / LPGCurve[2]) + LPGCurve[0]));
  return percentage * 1000000;
}

int MQ2::getPpmCO()
{
  float ratio = this -> getRatio();
  float percentage = pow(10, ((log(ratio - COCurve[1]) / COCurve[2]) + COCurve[0]));
  return percentage * 1000000;
}

int MQ2::getPpmSmoke()
{
  float ratio = this -> getRatio();
  float percentage = pow(10, ((log(ratio - SmokeCurve[1]) / SmokeCurve[2]) + SmokeCurve[0]));
  return percentage * 1000000;
}

#endif // __GAS_SENSOR_H__
