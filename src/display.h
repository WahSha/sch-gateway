#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include "Grove_4Digit_Display.h"

#include <stdint.h>

class Display
{
  private:
    TM1637* tm1637;
  public:
    Display(int, int, int);
    void clear(void);
    void write(int8_t[4]);
    void write(int);
    void write(float);
};


#endif // _DISPLAY_H_
