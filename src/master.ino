/* Particle libraries */
#include "Particle.h"

#include <Wire.h>

/* Local libraries */
#include "AirQuality.h"
#include "gasSensor.h"
#include "light.h"
#include "motion.h"
#include "publish.h"
#include "Seeed_DHT11.h"
#include "sound.h"
#include "variables.h"
#ifdef BAROMETER_ON
#include "barometer.h"
#endif
#ifdef DISPLAY_ON
#include "display.h"
#endif
#ifdef MULTICHANNEL_GAS_SENSOR_ON
#include "MultiChannelGasSensor.h"
#endif

// Pin & addresses
#define DISPLAY_CLK D12
#define DISPLAY_DIO D11
#define DISPLAY_BRIGHT 4
#define MOTION_PIN D4
#define AIR_QUALITY_PIN A4
#define GAS_SENSOR_PIN A2
#define LIGHT_SENSOR_PIN A5
#define SOUND_SENSOR_PIN A0
#define TEMP_HUM_SENSOR_PIN A3
#define MULTICHANNEL_GAS_SENSOR_ADDRESS 0x04
#define BAROMETER_ADDRESS 0x76

/* Global variables */
String project = "dbdlab"; // project id
String serial = System.deviceID(); // device serial number (hex)
String name = ""; // device name in cloud
String topicRoot = ""; // topic root for publish calls
int startTime; // device start time
int cycle; // number of current cycle
int runtime; // device runtime
int loopEnd; // end time of last cycle
Publish publish;

/* Sensors */
AirQualitySensor airQualitySensor(AIR_QUALITY_PIN);
MQ2 mq2(GAS_SENSOR_PIN);
DHT tempHumSensor(TEMP_HUM_SENSOR_PIN);
#ifdef DISPLAY_ON
Display display(DISPLAY_CLK, DISPLAY_DIO, DISPLAY_BRIGHT);
#endif

void setup()
{
  // Turn on status LED during setup
  pinMode(D7, OUTPUT);
  digitalWrite(D7, HIGH);

  // Initialize starting time, runtime and cycles counter
  startTime = Time.now();
  runtime = 0;
  cycle = 0;

  // Prepare sensor pins
  pinMode(MOTION_PIN, INPUT);
  pinMode(AIR_QUALITY_PIN, INPUT);
  pinMode(GAS_SENSOR_PIN, INPUT);
  pinMode(LIGHT_SENSOR_PIN, INPUT);
  pinMode(SOUND_SENSOR_PIN, INPUT);
  pinMode(TEMP_HUM_SENSOR_PIN, INPUT);

  // Prepare sensors
  tempHumSensor.begin();
  #ifdef MULTICHANNEL_GAS_SENSOR_ON
  gas.begin(MULTICHANNEL_GAS_SENSOR_ADDRESS);
  gas.powerOn();
  #endif

  // Get device name
  #ifdef PUB_NAME
  Particle.subscribe("particle/device/name", handlerName);
  Particle.publish("particle/device/name");
  while (name == "") delay(500);
  Particle.unsubscribe();
  #endif
  #ifndef PUB_NAME
  name = serial;
  #endif

  // Set topicRoot
  publish.setTopicRoot(project + "/" + name + "/");

  // Connect to MQTT if enabled
  #ifdef PUB_MQTT
  srand (startTime);
  int r = rand() % 99999 + 1;
  while(!publish.MQTTconnect(serial + String(r), project + ":debugger", "debugger")) {
    r = rand() % 99999 + 1;
    delay(500);
  }
  #endif

  // Setup event handlers
  System.on(reset+reset_pending, handlerReset);
  System.on(firmware_update+firmware_update_pending, handlerReset);

  // Publish serial, name, repo branch, build version, start time, mac address, ssid
  publish.publish("serial", serial);
  publish.publish("name", name);
  publish.publish("branch", branch);
  publish.publish("version", version);
  publish.publish("start_time", startTime);
  byte mac[6];
  WiFi.macAddress(mac);
  char sMac[17];
  sprintf(sMac, "%X:%X:%X:%X:%X:%X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  publish.publish("mac", String(sMac));
  publish.publish("ssid", WiFi.SSID());
  publish.publish("rssi", int(WiFi.RSSI()));
  publish.publish("transmission_interval", TRANSMISSION_INTERVAL);

  // Turn off status LED
  digitalWrite(D7, LOW);

  // Initialize loopEnd
  loopEnd = Time.now() - (TRANSMISSION_INTERVAL/1000);
}

void loop()
{
  #ifdef PUB_MQTT
  // Use MQTT client.loop() to keep connection alive
  publish.MQTTloop();
  #endif
  if (Time.now() < (loopEnd + (TRANSMISSION_INTERVAL/1000))) return;

  // Update runtime and cycle counter and initialize loop timer
  runtime = Time.now() - startTime;
  cycle++;

  #ifdef DISPLAY_ON
  // Write runtime on display
  display.write(runtime);
  #endif

  // Every 20 cycles publish runtime and cycles completed
  if (cycle % 20 == 1) {
    publish.publish("runtime", runtime);
    publish.publish("cycles", cycle - 1);
  }

  // Get sensor data
  int motion = getMotion(MOTION_PIN);
  int pollution = airQualitySensor.getQuality();
  int mq2LPG = mq2.getPpmLPG();
  int mq2CO = mq2.getPpmCO();
  int mq2Smoke = mq2.getPpmSmoke();
  int light = getLight(LIGHT_SENSOR_PIN);
  int sound = getSound(SOUND_SENSOR_PIN);
  int humidity = tempHumSensor.getHumidity();
  int temperature = tempHumSensor.getTemperature();
  #ifdef MULTICHANNEL_GAS_SENSOR_ON
  float C2H5OH = gas.measure_C2H5OH();
  float C3H8 = gas.measure_C3H8();
  float C4H10 = gas.measure_C4H10();
  float CH4 = gas.measure_CH4();
  float multiCO = gas.measure_CO();
  float H2 = gas.measure_H2();
  float NH3 = gas.measure_NH3();
  float NO2 = gas.measure_NO2();
  #endif
  #ifdef BAROMETER_ON
  float baro[3] ; getBarometer(BAROMETER_ADDRESS, baro);
  #endif

  // Publish sensor data with split delay
  publish.publish("tm2291_motion", motion);
  publish.publish("mp503_pollution", pollution);
  publish.publish("mq2_lpg", mq2LPG);
  publish.publish("mq2_co", mq2CO);
  publish.publish("mq2_smoke", mq2Smoke);
  publish.publish("lm386_sound", sound);
  publish.publish("gl5528_light", light);
  if (humidity != 0 && humidity != 255) publish.publish("dht11_humidity", humidity);
  if (temperature != 0 && temperature != 255) publish.publish("dht11_temperature", temperature);
  #ifdef MULTICHANNEL_GAS_SENSOR_ON
  if (C2H5OH >= 0) publish.publish("mics6814_c2h5oh", C2H5OH);
  if (C3H8 >= 0) publish.publish("mics6814_c3h8", C3H8);
  if (C4H10 >= 0) publish.publish("mics6814_c4h10", C4H10);
  if (CH4 >= 0) publish.publish("mics6814_ch4", CH4);
  if (multiCO >= 0) publish.publish("mics6814_co", multiCO);
  if (H2 >= 0) publish.publish("mics6814_h2", H2);
  if (NH3 >= 0) publish.publish("mics6814_nh3", NH3);
  if (NO2 >= 0) publish.publish("mics6814_no2", NO2);
  #endif
  #ifdef BAROMETER_ON
  if (baro[0] != 0) publish.publish("hp206c_temperature", baro[0]);
  if (baro[1] != 0) publish.publish("hp206c_pressure", baro[1]);
  if (baro[2] != 0) publish.publish("hp206c_altitude", baro[2]);
  #endif

  // Set loop end time
  loopEnd = Time.now();
}

// Save data to name variable for "particle/device/name" subscription
void handlerName(String topic, String data) { name = data; }

// Disconnect MQTT (if enabled)
void handlerReset()
{
  #ifdef PUB_MQTT
  publish.MQTTdisconnect();
  #endif
}
