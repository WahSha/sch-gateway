#include "display.h"
#include <math.h>

#define CLEAR 16

Display::Display(int clk, int dio,int brighteness = 5)
{
  tm1637 = new TM1637(clk, dio);
  tm1637 -> init();
  tm1637 -> set(brighteness); //BRIGHT_TYPICAL = 2,BRIGHT_DARKEST = 0,BRIGHTEST = 7;
  tm1637 -> point(POINT_OFF);
  this -> clear();
}

void Display::clear()
{
  for (int j = 3; j >= 0; j--) tm1637 -> display(j, CLEAR);
}

void Display::write(int8_t data[4])
{
  for (int i = 0; i < 4; i++) tm1637 -> display(i, data[i]);
}

void Display::write(int n)
{
  if (n >= 10000){
    int l = floor(log10(n));
    if (l >= 100) {
      for (int i = 0; i < 4; i++) tm1637 -> display(i, 14);
      return;
    }

    int j = 3;
    for (int ltmp = l; ltmp > 0; j--) {
      tm1637 -> display(j, ltmp % 10);
      ltmp /= 10;
    }

    tm1637 -> display(j, 14);
    n /= pow(10, l - (3-j));
    j--;
    for (; j >= 0; j--) {
      tm1637 -> display(j, n % 10);
      n /= 10;
    }
  } else {
    for (int j = 3; j >= 0; j--) {
      n != 0 ? tm1637 -> display(j, n % 10) : tm1637 -> display(j, CLEAR);
      n /= 10;
    }
  }
}

void Display::write(float n)
{
  this -> write(int(n));
}
