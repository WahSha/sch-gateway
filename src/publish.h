#ifndef __PUBLISH_H__
#define __PUBLISH_H__

#include "Particle.h"

#define PUB_MQTT
#undef PUB_PARTICLE

#ifdef PUB_MQTT
#include "MQTT/MQTT.h"
#include <MQTT.h>
#endif

class Publish
{
  private:
    MQTT* client;
    String topicRoot;
  public:
    Publish(String);
    Publish(void);
    void setTopicRoot(String);
    void publish(String, int);
    void publish(String, float);
    void publish(String, String);
    void publish(String, char*);
    void publish(String, const char*);
    void publish(String, bool);
    #ifdef PUB_MQTT
    bool MQTTconnect(String, String, String);
    void MQTTdisconnect(void);
    bool MQTTisConnected(void);
    void MQTTloop(void);
    #endif
};

#endif // __PUBLISH_H__
