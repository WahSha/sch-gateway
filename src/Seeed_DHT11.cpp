/* Seeed_DHT11 library
 *
 * MIT license
 *
 * */

#include "Seeed_DHT11.h"

#include "Arduino.h"
#include "math.h"

DHT::DHT(u8 pin, u8 count)
{
  _pin = pin;
  _count = count;
  firstreading = true;
}

void DHT::begin(void)
{
  // set up the pins!
  pinMode(_pin, INPUT);
  pinSetFast(_pin);
  _lastreadtime = 0;
}

int DHT::readTemperature()
{
  if (read()) return int(data[2]);
  else return NAN;
}

int DHT::getHumidity() { return readHumidity(); }

int DHT::getTemperature() { return readTemperature(); }

int DHT::readHumidity(void)
{
  if (read()) return int(data[0]);
  else return NAN;
}

boolean DHT::read(void)
{
  u8 laststate = HIGH;
  u8 counter = 0;
  u8 j = 0, i;
  u32 currenttime;

  // Check if sensor was read less than two seconds ago and return early
  // to use last reading.
  currenttime = millis();
  if (currenttime < _lastreadtime) {
    // ie there was a rollover
    _lastreadtime = 0;
  }
  if (!firstreading && ((currenttime - _lastreadtime) < 2000)) {
    return true; // return last correct measurement
    //    delay(2000 - (currenttime - _lastreadtime));
  }
  firstreading = false;
  _lastreadtime = millis();

  data[0] = data[1] = data[2] = data[3] = data[4] = 0;

  // pull the pin high and wait 250 milliseconds
  pinSetFast(_pin);
  delay(250);

  // send begin signal
  pinMode(_pin, OUTPUT);
  pinResetFast(_pin);
  delay(20);
  noInterrupts();
  pinSetFast(_pin);
  delayMicroseconds(40);
  pinMode(_pin, INPUT);

  // read high/low status
  for (i = 0; i < MAXTIMINGS; i++) {
    counter = 0;
    while (pinReadFast(_pin) == laststate) {
      counter++;
      delayMicroseconds(1);
      if (counter == 255) {
        break;
      }
    }
    laststate = pinReadFast(_pin);

    if (counter == 255)
      break;

    // ignore first 3 transitions,it's response signal
    if ((i >= 4) && (i % 2 == 0)) {
      // shove each bit into the storage bytes
      data[j / 8] <<= 1;
      if (counter > _count)
        data[j / 8] |= 1;
      j++;
    }
  }

  interrupts();

  // Verify that the data is correct
  if ((j >= 40) && (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF))) {
    return true;
  }

  return false;
}
