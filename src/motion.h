#ifndef __MOTION_H__
#define __MOTION_H__

int getMotion(uint8_t pin) { return digitalRead(pin); }

#endif // __MOTION_H__
