#include "publish.h"

Publish::Publish(String topicRootArg)
{
  topicRoot = topicRootArg;
  #ifdef PUB_MQTT
  client = new MQTT("platform.pulselabs.dk", 1883, NULL);
  #endif
}

Publish::Publish()
{
  topicRoot = "";
  #ifdef PUB_MQTT
  client = new MQTT("platform.pulselabs.dk", 1883, NULL);
  #endif
}

void Publish::setTopicRoot(String topicRootArg) { topicRoot = topicRootArg; }

void Publish::publish(String topic, int payload)
{
  #ifdef PUB_MQTT
  client -> publish(topicRoot + topic, ("{\"value\":" + String(payload) + "}").c_str());
  #endif
  #ifdef PUB_PARTICLE
  Particle.publish(topicRoot + topic, String(payload));
  #endif
}

void Publish::publish(String topic, float payload)
{
  #ifdef PUB_MQTT
  client -> publish(topicRoot + topic, ("{\"value\":" + String(payload) + "}").c_str());
  #endif
  #ifdef PUB_PARTICLE
  Particle.publish(topicRoot + topic, String(payload));
  #endif
}

void Publish::publish(String topic, String payload)
{
  #ifdef PUB_MQTT
  client -> publish(topicRoot + topic, ("{\"value\":\"" + payload + "\"}").c_str());
  #endif
  #ifdef PUB_PARTICLE
  Particle.publish(topicRoot + topic, payload);
  #endif
}

void Publish::publish(String topic, char* payload)
{
  this -> publish(topic, String(payload));
}

void Publish::publish(String topic, const char* payload)
{
  this -> publish(topic, String(payload));
}

void Publish::publish(String topic, bool payload)
{
  String payloadFormat = payload ? "true" : "false";

  #ifdef PUB_MQTT
  client -> publish(topicRoot + topic, ("{\"value\":" + payloadFormat + "}").c_str());
  #endif
  #ifdef PUB_PARTICLE
  Particle.publish(topicRoot + topic, payloadFormat);
  #endif
}

#ifdef PUB_MQTT
bool Publish::MQTTconnect(String id, String login, String secret) {
  if (client -> connect(id, login, secret)) {
    this -> publish("console", "connected");
    client -> subscribe(topicRoot + "control");
    return true;
  } else {
    return false;
  }
}

void Publish::MQTTdisconnect()
{
  if (client -> isConnected()) {
    this -> publish("console", "disconnect");
    client -> disconnect();
  }
}

bool Publish::MQTTisConnected() { return client -> isConnected(); }

void Publish::MQTTloop() { client -> loop(); }
#endif
